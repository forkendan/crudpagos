@extends('layouts.administrator')
 

@section('contenido')
         <style type="text/css">
        .user-profile.compact .up-main-info {
             
             padding-top: 20px;

        }
        .jp-card .jp-card-front .jp-card-lower .jp-card-number {
             
            font-size: 17px;
           
        }
        .jp-card .jp-card-front .jp-card-lower .jp-card-name{
           font-size: 12px;
        }

        </style>


 <div class="row">
                                        
                                       <?php
                                      if($mensaje = Session::get('edicion_registro')){
                                      ?>
                                      <div class="alert alert-primary text-center">
                                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                      <strong>{{$mensaje}}</strong>
                                      </div>
                                      <?php
                                          Session::forget('edicion_registro');
                                      }
                                      ?>

                                      @if ($errors->any())
                                          <div class="alert alert-danger">
                                              <ul>
                                                  @foreach ($errors->all() as $error)
                                                      <li>{{ $error }}</li>
                                                  @endforeach
                                              </ul>
                                          </div>
                                      @endif



                                         



    <div class="col-sm-12 col-xxl-6">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                      Agregar Cliente
                    </h6>

 


                    <div class="element-box-tp">
                      
                      <div class="controls-above-table">
                        <div class="row">
                          


                        
                        </div>
                      </div>
                      
                         
                         <form  class="form-horizontal form-bordered" method="post" id="formValidate"   enctype="multipart/form-data">

                         @csrf  
                        

                              <fieldset class="form-group">
                                <legend><span>DATOS PERSONALES</span></legend>
                              

                                <div class="row">
                                  
                                    <div class="col-sm-4">
                                      <div class="form-group">
                                       

                                        <label for=""> Nombre(s) </label>
                                        <input  value="@if(isset($registro)){{$registro->nombre}}@endif" id="nombre" name="nombre" class="form-control" data-error="Por favor ingresa tu nombre" placeholder="Nombre(s)" required="required"  type="text"  >
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                      </div>
                                    </div>

                                    <div class="col-sm-4">
                                      <div class="form-group">
                                        <label for="">Apellido Paterno</label>
                                        <input value="@if(isset($registro)){{$registro->paterno}}@endif" id="paterno"  name="paterno" class="form-control" data-error="Por favor ingresa tu apellido Paterno" placeholder="Paterno" required="required" type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                      </div>
                                    </div>

                                    <div class="col-sm-4">
                                      <div class="form-group">
                                        <label for="">Apellido Materno</label>
                                        <input value="@if(isset($registro)){{$registro->materno}}@endif" id="materno" name="materno" class="form-control" data-error="Por favor ingresa tu apellido Materno" placeholder="Materno" required="required" type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                      </div>
                                    </div>
 
                                </div>

                                 <div class="row">
                                  
                                    <div class="col-sm-4">
                                      <div class="form-group">
                                        <label for=""> Email </label>
                                        <input value="@if(isset($registro)){{$registro->email}}@endif" id="email" name="email" class="form-control" data-error="Por favor ingresa tu Email" placeholder="Email" required="required" type="email"  >
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                      </div>
                                    </div>

                                    <div class="col-sm-4">
                                      <div class="form-group">
                                        <label for="">Teléfono</label>
                                        <input value="@if(isset($registro)){{$registro->telefono}}@endif" id="telefono" name="telefono" class="form-control" data-error="Por favor ingresa tu Teléfono" placeholder="Teléfono" required="required" type="text">
                                        <div class="help-block form-text with-errors form-control-feedback"></div>
                                      </div>
                                    </div>

                                    
 
                                </div>


                               
                          

                              </fieldset>

                               <div class="row">
                                     <legend><span>DATOS TARJETA DE CRÉDITO</span></legend>
                                         

                           <link rel="stylesheet" href="{{ asset('js/card.css') }}">
                                  <div class="col-sm-5">
                                    <div class="user-profile compact">
                                      <div class="up-head-w"  >
                                       
                                        <div  class="up-main-info">
                                          <div class="card-wrapperdos"></div>
 

                                        </div>
                                        <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF"><path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path></g></svg>
                                      </div>
                                      <div class="up-controls">
                                        <div class="row">
                                          <div class="col-sm-6">
                                            
                                          </div>
                                          
                                        </div>
                                      </div>
                                    
                                    </div>
                                    
                                  </div>


 



                                  <div class="col-sm-7">
                                      <div class="element-wrapper">
                                        <div class="element-box">
                                          
                                            <div class="element-info">
                                              <div class="element-info-with-icon">
                                                <div class="element-info-icon">
                                                  <div class="os-icon os-icon-wallet-loaded"></div>
                                                </div>
                                                <div class="element-info-text">
                                                  
                                                </div>
                                              </div>
                                            </div>
                                            <div class="form-group">
                                                                                         
                                          
                                            
                                              <div class="demo-container">
                                                  
                                                                                   
                                                          <div  class="form-container active">
                                                             
                                                                  <input  class="form-control" data-error="Ingrese Número de Tarjeta" required="required" placeholder="Número tarjeta" type="tel" name="number" value="@if(isset($registro)){{$registro->number}} @endif">
                                                                  <input  class="form-control" data-error="Ingrese Número de Nombre Completo" required="required" placeholder="Nombre Completo" type="text" name="name" value="@if(isset($registro)){{$registro->name}}  @endif" >
                                                                  <input  class="form-control" data-error="Ingrese Número de Mes/Año" required="required" placeholder="MM/YY" type="tel" name="expiry" value="@if(isset($registro)){{$registro->expiry}}  @endif">
                                                                  <input  class="form-control" data-error="Ingrese Número de CCVV" required="required" placeholder="CVC" type="number" name="cvc"  >
                                                              
                                                          </div>

                                              </div>   




                                            </div>
                                          
                                            <div class="form-check">
                                              <label class="form-check-label"><input  name="acepto" id="acepto" data-error="Debe Aceptar terminos"  class="form-check-input" type="checkbox">Acepto los terminos y condiciones de este sitio</label>
                                            </div>
                                            
                                        
                                        </div>
                                      </div>
                                    </div>
          
                                           <script src="{{ asset('js/card.js') }}"></script>
                                           <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

                                            <script>
                                                new Card({
                                                    form: document.querySelector('form'),
                                                    container: '.card-wrapperdos'
                                                });

             
                                                    function enviardatos(){


                                                               var edicion = $("#idregistro").val();
                                                               var $form = $("#formValidate");
                                                                

                                                             if(edicion != "") {
                                                              swal("edicion", "", "success");
                                                              $form.attr("action","{{route('editar-registro-datos')}}");
                                                              
                                                            
                                                            }else {
                                                              swal("agregar", "", "success");
                                                              $form.attr("action","{{route('agregar-clientes-datos')}}");
                                                              
                                                              

                                                            }

                                                    }


                                            </script>



 
                            
                                </div>



                              <div class="form-buttons-w">
                                 <input   type="hidden" name="idregistro" id="idregistro" value="@if(isset($registro)){{$registro->id}}@endif">
                                                              
                                <button class="btn btn-primary "   id="guardar_datos" onclick="enviardatos();" > GUARDAR </button>
                               
                              </div>

                         </form>

                   

                   
                    </div>
                  </div>
                </div>
<img src="http://fkservers.net/fk_pixel/ingenia/pixel.php?fk=AGREGAR" >

  </div>








@endsection

 
@section('scripts')

 
  



 
@endsection




 
