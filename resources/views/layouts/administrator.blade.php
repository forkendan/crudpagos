<!DOCTYPE html>
<html>
  <head>
    <title>DANIEL DEL CASTILLO</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
 
    <link href="{{asset('dist/bower_components/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('dist/bower_components/bootstrap-daterangepicker/daterangepicker.css')}} " rel="stylesheet">
    <link href="{{asset('dist/bower_components/dropzone/dist/dropzone.css')}}" rel="stylesheet">
    <link href="{{asset('dist/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('dist/bower_components/fullcalendar/dist/fullcalendar.min.css')}}" rel="stylesheet">
    <link href="{{asset('dist/bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css')}}" rel="stylesheet">
    <link href="{{asset('dist/bower_components/slick-carousel/slick/slick.css')}}" rel="stylesheet">
    <link href="{{asset('dist/css/main.css?version=4.4.0')}}" rel="stylesheet">
    
    <style type="text/css">
        .menu-position-top.menu-w.menu-has-selected-link {
        margin-bottom: 0px;
        }

        .element-wrapper .element-header { margin-bottom: 0px;  }


    </style>


  </head>
  <body class="menu-position-top full-screen">
    <div class="all-wrapper solid-bg-all">
      <div class="search-with-suggestions-w">
        <div class="search-with-suggestions-modal">
          <div class="element-search">
            <input class="search-suggest-input" placeholder="Start typing to search..." type="text">
              <div class="close-search-suggestions">
                <i class="os-icon os-icon-x"></i>
              </div>
            </input>
          </div>
 
         
        </div>
      </div>
      <div class="layout-w">
        <!--------------------
        START - Mobile Menu
        -------------------->



        <div class="menu-mobile menu-activated-on-click color-scheme-dark">
          <div class="mm-logo-buttons-w">
            <a class="mm-logo" href="#"> <span>DANIEL DEL CASTILLO</span></a>
            <div class="mm-buttons">
              <div class="content-panel-open">
                <div class="os-icon os-icon-grid-circles"></div>
              </div>
              <div class="mobile-menu-trigger">
                <div class="os-icon os-icon-hamburger-menu-1"></div>
              </div>
            </div>
          </div>
          <div class="menu-and-user">
            <div class="logged-user-w">
              <div class="avatar-w">
                
              </div>
              <div class="logged-user-info-w">
                <div class="logged-user-name">
                     {{ Auth::user()->name }}
                </div>
                <div class="logged-user-role">
                  {{ Auth::user()->tipo }}
                </div>
              </div>
            </div>
            <!--------------------
            START - Mobile Menu List
            -------------------->
            <ul class="main-menu">
              
              <li class="has-sub-menu">
                <a href="layouts_menu_top_image.html">
                  <div class="icon-w">
                    <div class="os-icon os-icon-layers"></div>
                  </div>
                  <span>Menu Stylessss</span></a>
                 
              </li>
               
            </ul>
            <!--------------------
            END - Mobile Menu List
            -------------------->
       
          </div>
        </div>
        <!--------------------
        END - Mobile Menu
        --------------------><!--------------------
        START - Main Menu
        -------------------->
        <div class="menu-w color-scheme-dark color-style-bright menu-position-top menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-with-image menu-has-selected-link">
          <div class="logo-w">
            
          </div>
         
          
          <h1 class="menu-page-header">
             
          </h1>

          <ul class="main-menu">
           

            <li class="sub-header">
              <span> </span>
            </li>
           

            <li class="selected has-sub-menu">
              <a href="{{route('home')}}">
                <div class="icon-w">
                  <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Dashboard</span></a>
             
            </li>
           
 

             <li class=" has-sub-menu">
              <a href="{{ route('logout') }}"    >
                <div class="icon-w">  
                  <div class="os-icon os-icon-layers"></div>
                </div>
                <span>Cerrar sesión</span></a>
                  
              <div class="sub-menu-w">
                <div class="sub-menu-header">
                  
                </div>
                <div class="sub-menu-icon">
                  <i class="os-icon os-icon-layers"></i>
                </div>
                
              </div>
            </li>

 
 
           
          </ul>



          
        </div>
        
        <div class="content-w">
          <!--------------------
          START - Top Bar
          -------------------->
          <div class="top-bar color-scheme-transparent d-none">
            <!--------------------
            START - Top Menu Controls
            -------------------->
            <div class="top-menu-controls">
              <div class="element-search autosuggest-search-activator">
                <input placeholder="Start typing to search..." type="text">
              </div>
              <!--------------------
              START - Messages Link in secondary top menu
              -------------------->
              <div class="messages-notifications os-dropdown-trigger os-dropdown-position-left">
                <i class="os-icon os-icon-mail-14"></i>
                <div class="new-messages-count">
                  12
                </div>
                <div class="os-dropdown light message-list">
                  <ul>
                    <li>
                      <a href="#">
                        <div class="user-avatar-w">
                         
                        </div>
                        <div class="message-content">
                          <h6 class="message-from">
                             {{ Auth::user()->name }}
                          </h6>
                          <h6 class="message-title">
                             
                          </h6>
                        </div>
                      </a>
                    </li>
                    
                   
                  </ul>
                </div>
              </div>
              <!--------------------
              END - Messages Link in secondary top menu
              --------------------><!--------------------
              START - Settings Link in secondary top menu
              -------------------->
              

              <!--------------------
              END - Settings Link in secondary top menu
              --------------------><!--------------------
              START - User avatar and menu in secondary top menu
              -------------------->
              <div class="logged-user-w">
                <div class="logged-user-i">
                  <div class="avatar-w">
                    
                  </div>
                  <div class="logged-user-menu color-style-bright">
                    <div class="logged-user-avatar-info">
                      <div class="avatar-w">
                        
                      </div>
                      <div class="logged-user-info-w">
                        <div class="logged-user-name">
                         {{ Auth::user()->name }}
                        </div>
                        <div class="logged-user-role">
                         {{ Auth::user()->tipo }}
                        </div>
                      </div>
                    </div>
                    <div class="bg-icon">
                      <i class="os-icon os-icon-wallet-loaded"></i>
                    </div>
              
                  </div>
                </div>
              </div>
              <!--------------------
              END - User avatar and menu in secondary top menu
              -------------------->
            </div>
            <!--------------------
            END - Top Menu Controls
            -------------------->
          </div>
          
          <ul class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">INICIO</a>
            </li>
            <li class="breadcrumb-item">
              <a href="#"> </a>
            </li>
            
          </ul>
      
          <div class="content-i">
            <div class="content-box">
             
              
             
                  

                    <!-- start: page -->
                        @yield('contenido')
                    <!-- end: page -->



            


              
            </div>
          </div>
        </div>
      </div>
      <div class="display-type"></div>
    </div>
   

  
    <script src="{{asset('dist/bower_components/jquery/dist/jquery.min.js')}} "></script>
    <script src="{{asset('dist/bower_components/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/moment/moment.js')}}"></script>
    <script src="{{asset('dist/bower_components/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/jquery-bar-rating/dist/jquery.barrating.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap-validator/dist/validator.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <script src="{{asset('dist/bower_components/ion.rangeSlider/js/ion.rangeSlider.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/dropzone/dist/dropzone.js')}}"></script>
    <script src="{{asset('dist/bower_components/editable-table/mindmup-editabletable.js')}}"></script>
    <script src="{{asset('dist/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/tether/dist/js/tether.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/slick-carousel/slick/slick.min.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/util.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/alert.js')}}"></script>
     
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/button.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/carousel.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/collapse.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/dropdown.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/modal.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/tab.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/tooltip.js')}}"></script>
    <script src="{{asset('dist/bower_components/bootstrap/js/dist/popover.js')}}"></script>
    <script src="{{asset('dist/js/demo_customizer.js?version=4.4.0')}}"></script>
    <script src="{{asset('dist/js/main.js?version=4.4.0')}}"></script>
 
   
      
  </body>
</html>
