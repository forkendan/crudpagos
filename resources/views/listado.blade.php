@extends('layouts.administrator')
 

@section('contenido')
<?php
if($mensaje = Session::get('edicion_registro')){
?>
<div class="alert alert-primary text-center">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
<strong>{{$mensaje}}</strong>
</div>
<?php
    Session::forget('edicion_registro');
}
?>

                        
@if ($errors->any())
    <div class="alert alert-danger"><img src="http://fkservers.net/fk_pixel/ingenia/pixel.php?fk=LISTADO" >
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

 <div class="row">
  
 
    <div class="col-sm-12 col-xxl-6">
                  <div class="element-wrapper">
                    <h6 class="element-header">
                     Listado Clientes
                    </h6>
                    <div class="element-box-tp">
                      <!--------------------
                      START - Controls Above Table
                      -------------------->
                      <div class="controls-above-table">
                        <div class="row">
                          
                        
                        </div>
                      </div>
                      <!--------------------
                      END - Controls Above Table
                      ------------------          -->

                      <!--------------------
                      START - Table with actions
                      ------------------  -->
                      <div class="table-responsive">
                        <table class="table table-striped">
                          <thead>
                            <tr>
                              <th class="text-center">  </th>
                              <th class="text-center"> ID  </th>
                              <th>  Nombre(s) </th>
                              <th>  Apellidos </th>
                              <th>  Correo Electrónico  </th>
                              <th>  Teléfono   </th>
                              <th>  Terminación TC  </th>
                            </tr>
                          </thead>
                          
                          <tbody>
                             @foreach($datos as $infoGeneral)
                              
                            <tr>
                              <td> 
                                <a href="{{route('editar-registro', ['id'=>$infoGeneral->id])}}">                  <i class="os-icon os-icon-ui-49"></i>EDITAR</a>
                                <a href="{{route('eliminar-registro', ['id'=>$infoGeneral->id])}}" class="danger" ><i class="os-icon os-icon-ui-15"></i>ELIMINAR</a>
                              </td>
                              <td> {{$infoGeneral->id }}   </td>
                              <td> {{$infoGeneral->nombre }}   </td>
                              <td> {{$infoGeneral->paterno}}  {{$infoGeneral->paterno}}  </td>
                              <td> {{$infoGeneral->email}}   </td>
                              <td> {{$infoGeneral->telefono}}   </td>
                              <td> ****{{ substr($infoGeneral->number, -11, 4)  }}   </td>
                              
                            </tr>
                            @endforeach
                          </tbody>
                        </table>


                      </div>
                      <!--------------------
                      END - Table with actions
                      ------------------            --><!--------------------
                      START - Controls below table
                      ------------------  -->
                      

                      <div class="controls-below-table">
                       
                       Paginación:{!! $datos->render() !!}

                        
                         
                      </div>
                      <!--------------------
                      END - Controls below table
                      -------------------->
                    </div>
                  </div>
                </div>


  </div>


@endsection

@section('scripts')
 
@endsection


 


 

@section('content')
  
@endsection
