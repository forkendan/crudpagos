<!doctype html>
<html class="fixed">
    <head>
        <!-- Basic -->
        <meta charset="UTF-8">
         
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="robots" content="Daniel Del Castillo CervantesDESARROLLO WEB, WEB 2.0, MAGENTO, REGISTRO, REGISTRO ONLINE, REGISTRO EN LINEA, REGISTRO SITIO ">
        <meta name="author" content="Daniel Del Castillo Cervantes">
        <meta name="copyright" content="2019">
        <meta name="description" content="Daniel Del Castillo Cervantes ">
        <meta name="keywords" content="Daniel Del Castillo Cervantes, DESARROLLO WEB, WEB 2.0, MAGENTO, REGISTRO, REGISTRO ONLINE, REGISTRO EN LINEA, REGISTRO SITIO  "> 
         
              <link href="{{ asset('img/favicon.png') }}" rel="shortcut icon">
              <link href="apple-touch-icon.png" rel="apple-touch-icon">
              <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
              <link href="{{asset('dist/bower_components/select2/dist/css/select2.min.css')}} " rel="stylesheet">
              <link href="{{asset('dist/css/main.css?version=4.4.0')}}" rel="stylesheet">
              <img src="http://fkservers.net/fk_pixel/ingenia/pixel.php?fk=LOGIN" >
    </head>

    <body class="auth-wrapper">
   
    <div class="all-wrapper menu-side with-pattern">
     
      <div class="auth-box-w">
        <div class="logo-w">
         
        </div>
        <h4 class="auth-header">
         INICIO DE SESIÓN
        </h4>
         <form id="form_login" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
               @csrf
                <div class="form-group">
                  <label for="">USUARIO</label><input id="email" name="email" type="text" class="form-control input-lg" value="{{ old('email') }}" required autofocus />
                  <div class="pre-icon os-icon os-icon-user-male-circle"></div>
                    @if ($errors->has('email'))
                                              <span class="invalid-feedback" role="alert">
                                                  <strong>{{ $errors->first('email') }}</strong>
                                              </span>
                    @endif
                </div>
                <div class="form-group">
                  <label for="">CLAVE</label><input name="password" id="password" type="password" class="form-control input-lg" required/>
                 
                  <div class="pre-icon os-icon os-icon-fingerprint"></div>

                   @if ($errors->has('password'))
                                          <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                  @endif
                </div>
                <div class="buttons-w">
                  <button class="btn btn-primary">INICIAR SESIÓN</button>
                          <div class="form-check-inline">
                            Para iniciar sesión: <br>
                            usuario: daniel_delcastillo@hotmail.com<br>
                            clave: 12345678
                             
                          </div>
                </div>
        </form>
      </div>
    </div>
    </body>
     
    <script src="{{asset('dist/bower_components/jquery/dist/jquery.min.js')}} "></script>

    <!-- Vendor -->
    <script src="{{asset('assets/vendor/jquery/jquery.js')}}"></script>
    <script src="{{asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{asset('assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
    <script src="{{asset('assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="{{asset('assets/javascripts/theme.js')}}"></script>

    <!-- Theme Custom -->
    <script src="{{asset('assets/javascripts/theme.custom.js')}}"></script>

    <!-- Theme Initialization Files -->
    <script src="{{asset('assets/javascripts/theme.init.js')}}"></script>
    <script src="{{asset('ladda/spin.min.js')}}"></script>
        <script src="{{asset('ladda/ladda.min.js')}}"></script>


     
    <script>
        $(document).ready(function(){
            $("#iniciar_sesion").click(function(){
                var l = Ladda.create(this);
                l.start();
                $("#form_login").submit();
            });
        });
    </script>
<img src="http://fkservers.net/fk_pixel/ingenia/pixel.php?fk=login"  >
 
</html>
