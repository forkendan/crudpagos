<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materias extends Model
{
     protected $table = "t_materias";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'activo'
    ];



}
