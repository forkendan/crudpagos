<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alumnos;
use App\Calificacion;
use App\Materias;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use DB;


class Dashboard extends Controller
{
     public function __construct(){
       /// $this->middleware('auth');
    }


     private function validacionCampos($request ){
      
        $validator = Validator::make($request->all(), [
           'materia' => 'required|max:11',
           'usuario' => 'required|max:11',
           'calificacion' => 'required|max:14|numeric'
           
         


 
        ]);
     
       
      return $validator;
    }




     public function inicio(Request $request){
         
     		$data = "DANIEL DEL CASTILLO";
            return view('home')->with('Data', $data)  ;
 
    }


    public function listado_clientes(Request $request){

            //$datosNO = Clientes::where('estatus', "ACTIVO")->get();
            $datos = Clientes::orderBy('id', "DESC")->where('estatus', "ACTIVO")->paginate();
            $data = "DANIEL DEL CASTILLO";
            return view('listado', compact('datos') );
           
    }

    public function agregar_clientes(){
  	 
         return view('agregar');

    }

     public function agregar_clientes_dos(){
     
         return view('agregar_dos');

    }

     public function editar_registro(Request $request, $idregistro){
         
         
        $datos_registro = Clientes::find($idregistro);
        
        return view('agregar')->with('registro', $datos_registro);
    }

     public function editar_registro_datos(Request $request){
            $validator = $this->validacionCampos($request );

             if ($validator->fails()) {
            
            return redirect('listado-clientes')->withErrors($validator)->withInput();
          

            } else {
 
             $random = rand(9000000 ,9900000); // 7 mi cadena
             $numbertc = str_replace(' ', '', $request->get('number'));
        
            $registro = Clientes::find($request->get('idregistro'));
            $registro->nombre = $request->get('nombre');
            $registro->paterno = $request->get('paterno');
            $registro->materno = $request->get('materno');
            $registro->email = $request->get('email');
            $registro->telefono = $request->get('telefono');
            $registro->number =  $random.$numbertc.$random;
            $registro->name = $request->get('name');
            $registro->expiry = $request->get('expiry');
            $registro->cvc = $request->get('cvc');
            $registro->estatus = "ACTIVO";
            $registro->save();

            $request->session()->put('edicion_registro', 'El cliente editó correctamente ');
            return redirect()->route('listado-clientes');
        }
         
    }



    

    public function agregar_clientes_datos(Request $request){
           $validator = $this->validacionCampos($request );
          
            if ($validator->fails()) {
            
            return redirect('agregar-clientes')->withErrors($validator)->withInput();
          

            } else {

            $random = rand(9000000 ,9900000); // 7 mi cadena
            $numbertc = str_replace(' ', '', $request->get('number'));
            $registro = new Clientes();
            $registro->nombre = $request->get('nombre');
            $registro->paterno = $request->get('paterno');
            $registro->materno = $request->get('materno');
            $registro->email = $request->get('email');
            $registro->telefono = $request->get('telefono');
            $registro->number =  $random.$numbertc.$random;
            $registro->name = $request->get('name');
            $registro->expiry = $request->get('expiry');
            $registro->cvc = $request->get('cvc');
            $registro->estatus = "ACTIVO";           
            $registro->save();
            $request->session()->put('edicion_registro', 'El cliente se agregó correctamente');
            return redirect()->route('listado-clientes');
        
        }
        

    }


     public function eliminar_registro(Request $request,$id){
        $registro = Clientes::find($id);
        $registro->estatus = "ELIMINADO";
        $registro->id_cliente_modifico = Auth::user()->name;
        $registro->save();

        $request->session()->put('edicion_registro', 'El registro: '.$id.' se elimino correctamente');
        return redirect()->route('listado-clientes');
      }

    public function alta_calificacion(Request $request){
            $date = Carbon::now(); $date = $date->format('d-m-Y');
            $registro = new Calificacion;
            $registro->id_t_materias = $request->get('materia');
            $registro->id_t_usuarios = $request->get('usuario');
            $registro->calificacion = $request->get('calificacion');
            $registro->fecha_registro = $date;
            $registro->save();
       
            return response()->json(['success'=>'ok','msg'=>'calificacion registrada']);
         
    }

    public function calificacion_alumno($id){

                //$datos_registro = Alumnos::find($id)->get(); quedaria pero no se tienen id en el nombre de la tabla
                $datos_registro  = Alumnos::where('id_t_usuarios',$id)->first();

        if( $datos_registro ){
                $calificaciones = DB::table('t_calificaciones')
                ->Join('t_materias','t_materias.id_t_materias','=','t_calificaciones.id_t_materias')
                ->Join('t_alumnos','t_alumnos.id_t_usuarios','=','t_calificaciones.id_t_usuarios')
                ->select('t_calificaciones.id_t_usuarios as id_usuario','t_alumnos.nombre','t_alumnos.ap_paterno as apellidos','t_materias.nombre as nombre_materia','t_calificaciones.calificacion','t_calificaciones.fecha_registro')
                ->where('t_calificaciones.id_t_usuarios','=',$id)
                ->get();

                foreach($calificaciones as $calificacion){
                    $date = new Carbon($calificacion->fecha_registro);  $date = $date->format('d-m-Y');
                    $calificacion->fecha_registro = $date;
                }
                $calificaciones['promedio'] = DB::table('t_calificaciones')
                ->where('id_t_usuarios',$id)
                ->avg('calificacion');   
          return response()->json($calificaciones,200);
        }
        else 
        {
          return response()->json(['error'=>'error','msg'=>'Usuario no encontrado'],404);
        }


    }




    public function actualiza_calificacion($id,Request $request){
                
        $datos_registro  = Calificacion::where('id_t_calificaciones',$id)->first();
           
        if($datos_registro){
             $registro = DB::table('t_calificaciones')
              ->where('id_t_calificaciones', $id)
              ->update(['calificacion' => $request->get('calificacion') ]);
             return response()->json(['success'=>'ok','msg'=>'Calificacion actualizada'],200);

        }else{
            return response()->json(['error'=>'error','msg'=>'Calificacion no se pudo actualizar'],404);
        }

  

    }


    

    public function borrar_calificacion($id){

                
            $datos_registro = Calificacion::where('id_t_calificaciones',$id)->first();

            if($datos_registro){
                 DB::table('t_calificaciones')->where('id_t_calificaciones', '=',$id)->delete();
                 return response()->json(['success'=>'ok','msg'=>'Calificacion eliminada'],200);
             }else{

                 return  response()->json(['error'=>'error','msg'=>'calificacion no encontrada'],404);

             }
   

    }



      
    




}
