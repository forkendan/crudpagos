<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumnos extends Model
{
    protected $table = "t_alumnos";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id_t_usuarios', 'nombre', 'ap_paterno','ap_materno','activo' 
    ];

}
