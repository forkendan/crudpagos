<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    
    protected $table = "t_calificaciones";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_t_materias', 'id_t_usuarios','calificacion','fecha_registro' 
    ];

    
}
