<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


 
 
Route::post('/alta-calificacion','Dashboard@alta_calificacion');
Route::get('/calificacion-alumno/{id}','Dashboard@calificacion_alumno');
Route::put('/actualiza-calificacion/{id}','Dashboard@actualiza_calificacion');
Route::delete('/borrar-calificacion/{id}','Dashboard@borrar_calificacion');



