<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(Auth::check()){
        return redirect()->route('home');
    }else{
        return view('auth.login');
    }
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('login', 'LoginDaniel@authenticate')->name('login');
Route::get('/dashboard', 'dashboard@inicio')->name('dashboard');
Route::get('/listado-clientes', 'dashboard@listado_clientes')->name('listado-clientes');
Route::get('/agregar-clientes', 'dashboard@agregar_clientes')->name('agregar-clientes');
Route::get('/agregar-clientesdos', 'dashboard@agregar_clientes_dos')->name('agregar-clientesdos');
Route::get('/editar-registro/{idregistro}', 'dashboard@editar_registro')->name('editar-registro');
Route::get('/eliminar-registro/{id}', 'dashboard@eliminar_registro')->name('eliminar-registro');

Route::post('/agregar-clientes-datos', 'dashboard@agregar_clientes_datos')->name('agregar-clientes-datos');
Route::post('/editar-registro-datos', 'dashboard@editar_registro_datos')->name('editar-registro-datos');
Route::get('logout', 'Auth\LoginController@logout'); 